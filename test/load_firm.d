/+ dub.sdl:
   name "load_firm"
   dependency "firm-d" path="../"
   targetName "load_firm"
   targetPath "../bin"
+/
module load_firm;

import
    std.stdio, std.format, std.string;
import
    firm;

void main()
{
    ir_init();
    scope(exit)
        ir_finish();

    string v = "%d.%d.%d (rev %s)".format(
        ir_get_version_major,
        ir_get_version_minor,
        ir_get_version_micro,
        ir_get_version_revision.fromStringz
    );

    testIdent();
    stuff();

    writeln("loaded firm version: ", v);
}

void testIdent()
{
    ident* i = new_id_from_str("fantomas");
    assert(i !is null);
    assert(get_id_str(i).fromStringz == "fantomas");
}

void stuff()
{
    // create type of `uint fun()`
    ir_type *proto = new_type_method(0, 1, false, cc_cdecl_set,
        mtp_additional_properties.mtp_no_property);
    ir_type *uint_ = new_type_primitive(mode_Iu);
    set_method_res_type(proto, 0, uint_);
    ident *name = ir_platform_mangle_global("fun");
    ir_entity* fun = new_entity(get_glob_type(), name, proto);


    //
    ir_graph *fun_graph = new_ir_graph(fun, 0);
    set_current_ir_graph(fun_graph);

    // create `return 1 + 2;`
    ir_node* one = new_tarval_from_long(1, mode_Iu).new_Const;
    ir_node* two = new_tarval_from_long(2, mode_Iu).new_Const;
    ir_node* one_plus_two = new_Add(one, two);
    ir_node* ret = new_Return(get_store(), 1, &one_plus_two);
    add_immBlock_pred(get_irg_end_block(fun_graph), ret);
    mature_immBlock(get_r_cur_block(fun_graph));

    // finalize the construction
    irg_finalize_cons(fun_graph);

    // looks like the add is optimized implictly...
    be_main(stdout.getFP(), "sdf.asm");
}

