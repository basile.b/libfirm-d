#!/bin/bash

FIRM_HEAD_1="0"
## update the C library
if [ -d "c" ]; then
    cd c
    PULL_DATE_FILE="../.last_firm_pull"
    PREV_PULL_DATE=""
    if [ -f "$PULL_DATE_FILE" ]; then
        PREV_PULL_DATE=$(cat "$PULL_DATE_FILE")
    fi
    CURR_PULL_DATE=$(date '+%d/%m/%Y')
    FIRM_HEAD_1=$(git rev-parse HEAD)
    git checkout master
    if [ ! "$CURR_PULL_DATE" = "$PREV_PULL_DATE" ]; then
        git pull
    else
        echo "already pulled FIRM on "$CURR_PULL_DATE
    fi
    echo $CURR_PULL_DATE > $PULL_DATE_FILE
else
    git clone https://github.com/libfirm/libfirm.git c
    cd c
fi
if [ -z ${FIRM_REVISION} ]; then
    FIRM_REVISION=master
fi
git checkout ${FIRM_REVISION}
FIRM_HEAD_2=`git rev-parse HEAD`
if [ "$FIRM_HEAD_1" = "$FIRM_HEAD_2" ]; then
    if [ -f "../src/firm/package.d" ]; then
        echo "skipped firm rebuilding..."
        exit
    fi
fi

NUM_CPU=$((`nproc` + 1))
make -j${NUM_CPU}
cd ..

## copy the includes to get a proper layout for the D package
rm -rf "./temp"
rm -rf "./src"
mkdir "temp"
cp -rf "./c/include/libfirm/." "./temp/"
rm -rf "./temp/adt/"
cp "./c/build/gen/include/libfirm/nodes.h" "./temp"

## fix https://github.com/libfirm/libfirm/issues/14
FIXED=`grep 'include "firm_types.h"' "./temp/nodes.h"`
if [ -z ${FIXED} ]; then
    sed -i '5,8d' "./temp/nodes.h"
    sed -i '5i #include <stddef.h>\n#include "firm_types.h"\n\n#include "begin.h"\n\n' "./temp/nodes.h"
fi

## fix for styx CI where ~/bin is not in PATH
DSTEP=$(which "dstep")
if [ -z "$DSTEP" ]; then
    DSTEP="~/bin/dstep"
fi

## make the D interfaces
## FIXME: "dub run dstep -- ..." does not work
H_FILES=`find "./temp" -type f -name *.h`
$DSTEP ${H_FILES}\
 -o "./src"\
 --package="firm"\
 --global-import="core.vararg"\
 --global-import="core.stdc.stddef"\
 --collision-action=ignore
mv "./src/temp" "./src/firm"
rm -rf "./temp"

## add the public imports to the package file
mv "./src/firm/firm.d" "./src/firm/package.d"
D_FILES=`find "./src/firm" -type f -name *.d`
for D_FILE in ${D_FILES} ; do
    if [ ${D_FILE} != "./src/firm/package.d" ]; then
        MODULE=`grep -o "module firm.[a-z,A-Z,0-9,_.]*" ${D_FILE}`
        IMPORTS+="public "${MODULE/module/import}$';\r\n'
    fi
done
echo ${IMPORTS} >> "./src/firm/package.d"
