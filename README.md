## Firm-d [![image](https://travis-ci.org/Basile-z/libfirm-d.svg?branch=master)](https://travis-ci.org/Basile-z/libfirm-d)

[D](https://www.dlang.org) bindings to [libfirm](https://github.com/libfirm/libfirm), a library for intermediate representation of source code, based on [SSA](https://en.wikipedia.org/wiki/Static_single_assignment_form).

Since this is not a mainstream library only linking of the static library is supported. 
See DUB scripts in `./test`, it's as simple as passing the library file as it if was a D source.

### Installation & Usage

- Windows is not supported
- [dstep](https://github.com/jacob-carlborg/dstep) must be build and visible in the PATH.

The first time:

- `dub build` to generate
- `dub build` again because D sources were not visible the first time

### Update

Update is automatic.
To use a particular libfirm revision set the environment variable `FIRM_REVISION` to a git tag, branch or commit.

### Repository structure

| Directory     | Content                                                      |
|---------------|--------------------------------------------------------------|
| `./`          | This README, LICENSE, utility scripts.                       |
| `./c`         | Firm C sources as a submodule, auto generated when building  |
| `./src`       | Firm D bindings, auto generated                              |
| `./test`      | simple DUB scripts used to verify that things link correctly |
